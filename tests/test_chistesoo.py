#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import unittest

from xml.sax import make_parser
import chistesoo

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_text = """Calificación: regular.
 Respuesta: Te veo en la esquina.
 Pregunta: ¿Qué le dice una pared a otra pared?

Calificación: malo.
 Respuesta: Si se tiraran de frente, caerían en el barco.
 Pregunta: ¿Por qué los buzos se tiran de espaldas?

Calificación: malisimo.
 Respuesta: Que se te pasa el tiempo volando.
 Pregunta: ¿Qué es lo mejor de ser piloto?

"""

result_text8 = """Calificación: buenisimo.
 Respuesta: Uno oso cartesiano tras un cambio de coordenadas.
 Pregunta: ¿Qué es uno oso polar?

Calificación: buenisimo.
 Respuesta: Porque tienen muchos problemas.
 Pregunta: ¿Por qué siempre están preocupados los libros de mates?

Calificación: bueno.
 Respuesta: Con lo grande que eres... y no sales de noche.
 Pregunta: ¿Qué le dijo la Luna al Sol?

Calificación: regular.
 Respuesta: Te veo en la esquina.
 Pregunta: ¿Qué le dice una pared a otra pared?

Calificación: malo.
 Respuesta: Si se tiraran de frente, caerían en el barco.
 Pregunta: ¿Por qué los buzos se tiran de espaldas?

Calificación: malo.
 Respuesta: En que el pegamento, pega, y el avión, despega.
 Pregunta: ¿En qué son diferentes el pegamento y un avión?

Calificación: malisimo.
 Respuesta: Te espero en el café.
 Pregunta: ¿Qué le dijo el azúcar a la leche?

Calificación: malisimo.
 Respuesta: Que se te pasa el tiempo volando.
 Pregunta: ¿Qué es lo mejor de ser piloto?

"""

result_list = [
    {'score': 'regular',
     'answer': "Te veo en la esquina.",
     'question': "¿Qué le dice una pared a otra pared?"
    },
    {'score': 'malo',
     'answer': "Si se tiraran de frente, caerían en el barco.",
     'question': "¿Por qué los buzos se tiran de espaldas?"
    },
    {'score': 'malisimo',
     'answer': "Que se te pasa el tiempo volando.",
     'question': "¿Qué es lo mejor de ser piloto?"
    }
]

class TestHumor(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)

    def test_object(self):
        jokes = chistesoo.Humor("chistes.xml")
        self.assertEqual(result_list, jokes.jokes())

class TestChistes(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)
        self.stdout = StringIO()

    def test_main(self):
        with contextlib.redirect_stdout(self.stdout):
            chistesoo.main("chistes.xml")
        output = self.stdout.getvalue()
        self.assertEqual(result_text, output)

    def test_main8(self):
        with contextlib.redirect_stdout(self.stdout):
            chistesoo.main(os.path.join("tests", "chistes8.xml"))
        output = self.stdout.getvalue()
        self.assertEqual(result_text8, output)

    def test_bad_root(self):
        with self.assertRaises(Exception) as exc:
            chistesoo.main(os.path.join("tests", "chistes_bad_root.xml"))
            self.assertTrue('Root element is not humor' in exc.exception)

if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
